﻿using System.Collections.Generic;
using System.Configuration;
using VendasApi.Helpers;
using Microsoft.Extensions.Configuration;
using RestSharp;

/// <summary>
/// Classe genérica para controle de apis.
/// </summary>
public class Api
{
    #region ATRIBUTOS

    private string _urlApi { get; set; }

    #endregion

    #region CONSTRUTORES

    /// <summary>
    /// Mapeia e busca a url 
    /// </summary>
    public Api()
    {
        this._urlApi = new AppSettingsManager().GetValue(this.GetType().Name);
    }

    #endregion

    #region METODOS

    /// <summary>
    /// Método de requisição GET.
    /// </summary>
    /// <param name="rotaMetodo"></param>
    /// <returns></returns>
    public IRestResponse Get(string rotaMetodo)
    {
        string url = _urlApi + rotaMetodo;

        RestClient client = new RestClient(url);
        RestRequest request = new RestRequest(Method.GET);

        client.Timeout = -1;
        request.Timeout = -1;

        request.AddHeader("cache-control", "no-cache");
        request.AddHeader("Content-Type", "application/json");

        return client.Execute(request);
    }

    /// <summary>
    /// Método de requisição POST.
    /// </summary>
    /// <param name="rotaMetodo"></param>
    /// <param name="param"></param>
    /// <param name="extraHeaders"></param>
    /// <returns></returns>
    public IRestResponse Post(string rotaMetodo, string param, Dictionary<string, string> extraHeaders = null)
    {
        string url = _urlApi + rotaMetodo;

        RestClient client = new RestClient(url);
        RestRequest request = new RestRequest(Method.POST);

        request.AddHeader("cache-control", "no-cache");
        request.AddHeader("Content-Type", "application/json");
        request.AddParameter("application/json", param, ParameterType.RequestBody);

        if (extraHeaders != null)
        {
            foreach (var dic in extraHeaders)
            {
                request.AddHeader(dic.Key, dic.Value);
            }
        }

        return client.Execute(request);
    }

    /// <summary>
    /// Método de requisição PUT.
    /// </summary>
    /// <param name="rotaMetodo"></param>
    /// <param name="param"></param>
    /// <returns></returns>
    public IRestResponse Put(string rotaMetodo, string param)
    {
        string url = _urlApi + rotaMetodo;

        RestClient client = new RestClient(url);
        RestRequest request = new RestRequest(Method.PUT);

        request.AddHeader("cache-control", "no-cache");
        request.AddHeader("Content-Type", "application/json");
        request.AddParameter("application/json", param, ParameterType.RequestBody);

        return client.Execute(request);
    }

    /// <summary>
    /// Método de requisição DELETE.
    /// </summary>
    /// <param name="rotaMetodo"></param>
    /// <returns></returns>
    public IRestResponse Delete(string rotaMetodo)
    {
        string url = _urlApi + rotaMetodo;

        RestClient client = new RestClient(url);
        RestRequest request = new RestRequest(Method.DELETE);

        request.AddHeader("cache-control", "no-cache");
        request.AddHeader("Content-Type", "application/json");

        return client.Execute(request);
    }

    #endregion
}