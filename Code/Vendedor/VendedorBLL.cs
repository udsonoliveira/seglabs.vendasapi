﻿using VendasApi.Helpers.Extensions;
using VendasApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace VendasApi.Code
{
    public class VendedorBLL
    {
        public IList<Vendedor> ConsultarVendedor(int id = 0)
        {
            try
            {
                var dados = new VendedorDAL().ConsultarVendedor();
                
                if(id > 0)
                {
                    dados = dados.Where(x => x.Id == id)
                                .ToList();
                }
                foreach(var vendedor in dados)
                {
                    vendedor.Ven_comissao = new VendaBLL().ConsultarVenda(0, vendedor.Id, DateTime.Now).Sum(x => x.Ven_total) * 5 / 100;
                }

                return dados;
            }
            catch(Exception e)
            {
                throw e.HandleException();
            }
        }

        public bool CadastrarVendedor(Vendedor vendedor)
        {
            try
            {
                var dados = new VendedorDAL().CadastrarVendedor(vendedor);
                return dados;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public bool AtualizarVendedor(Vendedor vendedor)
        {
            try
            {
                return new VendedorDAL().AtualizarVendedor(vendedor);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
    }
}
