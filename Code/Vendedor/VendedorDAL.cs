﻿using VendasApi.Database;
using VendasApi.Helpers.Extensions;
using VendasApi.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace VendasApi.Code
{
    public class VendedorDAL
    {
        /// <summary>
        /// Consultar Vendedor
        /// </summary>
        /// <returns></returns>
        public IList<Vendedor> ConsultarVendedor()
        {
            using (VendasContext ctx = new VendasContext())
            {
                var lista = ctx.Vendedores.ToList();
                return lista;
            }
        }

        /// <summary>
        /// Cadastrar Vendedor
        /// </summary>
        /// <param name="vendedor"></param>
        /// <returns></returns>
        public bool CadastrarVendedor(Vendedor vendedor)
        {
            var retorno = 0;

            using (var ctx = new VendasContext())
            {
                ctx.Vendedores.Add(vendedor);
                retorno = ctx.SaveChanges();
            }

            return retorno > 0;
        }

        /// <summary>
        /// Atualiza a entidade vendedor
        /// </summary>
        /// <param name="vendedor"></param>
        /// <returns></returns>
        public bool AtualizarVendedor(Vendedor vendedor)
        {
            using (VendasContext ctx = new VendasContext())
            {
                var uVendedor = ctx.Vendedores.Where(x => x.Id == vendedor.Id)
                                               .ToList()
                                               .FirstOrDefault();

                if (vendedor != null)
                {
                    uVendedor.MergeObject(vendedor);
                }

                ctx.Entry(uVendedor).State = EntityState.Modified;
                return ctx.SaveChanges() > 0;
            }
        }
    }
}
