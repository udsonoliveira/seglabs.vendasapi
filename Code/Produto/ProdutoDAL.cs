﻿using VendasApi.Database;
using VendasApi.Helpers.Extensions;
using VendasApi.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace VendasApi.Code
{
    public class ProdutoDAL
    {
        /// <summary>
        /// Consultar Produtos
        /// </summary>
        /// <returns></returns>
        public IList<Produto> ConsultarProduto()
        {
            using (VendasContext ctx = new VendasContext())
            {
                var lista = ctx.Produtos.ToList();
                return lista;
            }
        }

        /// <summary>
        /// Cadastrar Produto
        /// </summary>
        /// <param name="produto"></param>
        /// <returns></returns>
        public bool CadastrarProduto(Produto produto)
        {
            var retorno = 0;

            using (var ctx = new VendasContext())
            {
                ctx.Produtos.Add(produto);
                retorno = ctx.SaveChanges();
            }

            return retorno > 0;
        }

        /// <summary>
        /// Atualiza a entidade produto
        /// </summary>
        /// <param name="produto"></param>
        /// <returns></returns>
        public bool AtualizarProduto(Produto produto)
        {
            using (VendasContext ctx = new VendasContext())
            {
                var uProduto = ctx.Produtos.Where(x => x.Id == produto.Id)
                                               .ToList()
                                               .FirstOrDefault();

                if (produto != null)
                {
                    uProduto.MergeObject(produto);
                }

                ctx.Entry(uProduto).State = EntityState.Modified;
                return ctx.SaveChanges() > 0;
            }
        }
    }
}
