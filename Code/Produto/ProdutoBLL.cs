﻿using VendasApi.Helpers.Extensions;
using VendasApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace VendasApi.Code
{
    public class ProdutoBLL
    {
        public IList<Produto> ConsultarProduto(int id = 0)
        {
            try
            {
                var dados = new ProdutoDAL().ConsultarProduto();
                
                if(id > 0)
                {
                    return dados.Where(x => x.Id == id)
                                .ToList();
                }
                else 
                {
                    return dados;    
                }
            }
            catch(Exception e)
            {
                throw e.HandleException();
            }
        }

        public bool CadastrarProduto(Produto produto)
        {
            try
            {
                var dados = new ProdutoDAL().CadastrarProduto(produto);
                return dados;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public bool AtualizarProduto(Produto produto)
        {
            try
            {
                return new ProdutoDAL().AtualizarProduto(produto);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
    }
}
