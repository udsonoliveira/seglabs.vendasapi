﻿using VendasApi.Database;
using VendasApi.Helpers.Extensions;
using VendasApi.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace VendasApi.Code
{
    public class VendaDAL
    {
        /// <summary>
        /// Consultar Vendas
        /// </summary>
        /// <returns></returns>
        public IList<Venda> ConsultarVendas()
        {
            using (VendasContext ctx = new VendasContext())
            {
                var lista = ctx.Vendas
                               .Include(x => x.Ven_vendedor)
                               .ToList();
                return lista;
            }
        }

        /// <summary>
        /// Cadastrar Venda
        /// </summary>
        /// <param name="venda"></param>
        /// <returns></returns>
        public bool CadastrarVenda(Venda venda)
        {
            var retorno = 0;

            using (var ctx = new VendasContext())
            {
                ctx.Vendas.Add(venda);
                retorno = ctx.SaveChanges();
            }

            return retorno > 0;
        }

        /// <summary>
        /// Atualiza a entidade venda
        /// </summary>
        /// <param name="venda"></param>
        /// <returns></returns>
        public bool AtualizarVenda(Venda venda)
        {
            using (VendasContext ctx = new VendasContext())
            {
                var uVenda = ctx.Vendas.Where(x => x.Id == venda.Id)
                                               .ToList()
                                               .FirstOrDefault();

                if (venda != null)
                {
                    uVenda.MergeObject(venda);
                }

                ctx.Entry(uVenda).State = EntityState.Modified;
                return ctx.SaveChanges() > 0;
            }
        }
    }
}
