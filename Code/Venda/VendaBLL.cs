﻿using VendasApi.Helpers.Extensions;
using VendasApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace VendasApi.Code
{
    public class VendaBLL
    {
        public IList<Venda> ConsultarVenda(int id = 0, int vendedorId = 0, DateTime? Ven_data = null)
        {
            try
            {
                var dados = new VendaDAL().ConsultarVendas();
                
                if(id > 0)
                {
                    return dados.Where(x => x.Id == id)
                                .ToList();
                }
                else if(vendedorId > 0 && Ven_data != null)
                {
                    return dados.Where(x => (x.VendedorId == vendedorId && x.Ven_data.ToString("DD/MM/YYYY").Equals(Ven_data.GetValueOrDefault().ToString("DD/MM/YYYY"))))
                                .ToList();
                } 
                else if (vendedorId == 0 && Ven_data != null)
                {
                    return dados.Where(x => x.Ven_data.ToString("DD/MM/YYYY").Equals(Ven_data.GetValueOrDefault().ToString("DD/MM/YYYY")))
                                .ToList();
                }
                else
                {
                    return dados;    
                }
            }
            catch(Exception e)
            {
                throw e.HandleException();
            }
        }

        public bool CadastrarVenda(Venda venda)
        {
            try
            {
                var dados = new VendaDAL().CadastrarVenda(venda);
                return dados;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public bool AtualizarVenda(Venda venda)
        {
            try
            {
                return new VendaDAL().AtualizarVenda(venda);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
    }
}
