﻿using VendasApi.Database;
using VendasApi.Helpers.Extensions;
using VendasApi.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace VendasApi.Code
{
    public class VendaItemDAL
    {
        /// <summary>
        /// Consultar itens da venda
        /// </summary>
        /// <returns></returns>
        public IList<VendaItem> ConsultarVendaItens()
        {
            using (VendasContext ctx = new VendasContext())
            {
                var lista = ctx.VendaItens
                               .Include(x => x.VenIte_produto)
                               .Include(x => x.VenIte_venda)
                               .ToList();
                return lista;
            }
        }

        /// <summary>
        /// Cadastrar item
        /// </summary>
        /// <param name="vendaItem"></param>
        /// <returns></returns>
        public bool CadastrarVendaItem(VendaItem vendaItem)
        {
            var retorno = 0;

            using (var ctx = new VendasContext())
            {
                ctx.VendaItens.Add(vendaItem);
                retorno = ctx.SaveChanges();
            }

            return retorno > 0;
        }

        /// <summary>
        /// Atualiza a entidade item da venda
        /// </summary>
        /// <param name="vendaItem"></param>
        /// <returns></returns>
        public bool AtualizarVendaItem(VendaItem vendaItem)
        {
            using (VendasContext ctx = new VendasContext())
            {
                var uVendaItem = ctx.VendaItens.Where(x => x.Id == vendaItem.Id)
                                               .ToList()
                                               .FirstOrDefault();

                if (vendaItem != null)
                {
                    uVendaItem.MergeObject(vendaItem);
                }

                ctx.Entry(uVendaItem).State = EntityState.Modified;
                return ctx.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// Remove a entidade item da venda
        /// </summary>
        /// <param name="vendaItem"></param>
        /// <returns></returns>
        public bool RemoverVendaItem(VendaItem vendaItem)
        {
            using (VendasContext ctx = new VendasContext())
            {
                ctx.VendaItens.Remove(vendaItem);
                return ctx.SaveChanges() > 0;
            }
        }
    }
}
