﻿using VendasApi.Helpers.Extensions;
using VendasApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace VendasApi.Code
{
    public class VendaItemBLL
    {
        public IList<VendaItem> ConsultarVendaItem(int VendaId, int id = 0)
        {
            try
            {
                var dados = new VendaItemDAL().ConsultarVendaItens();
                
                if(id > 0)
                {
                    return dados.Where(x => (x.Id == id && x.VendaId == VendaId))
                                .ToList();
                }
                else
                {
                    return dados.Where(x => x.VendaId == VendaId)
                                .ToList();
                }
            }
            catch(Exception e)
            {
                throw e.HandleException();
            }
        }

        public bool CadastrarVendaItem(VendaItem vendaItem)
        {
            try
            {
                var dados = new VendaItemDAL().CadastrarVendaItem(vendaItem);
                if(dados)
                {
                    var venda = new VendaBLL().ConsultarVenda(vendaItem.VendaId).First();
                    venda.Ven_total = ConsultarVendaItem(vendaItem.VendaId).ToList().Sum(x => x.VenIte_subtotal);

                    dados = new VendaDAL().AtualizarVenda(venda);
                }
                return dados;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public bool AtualizarVendaItem(VendaItem vendaItem)
        {
            try
            {
                var dados = new VendaItemDAL().AtualizarVendaItem(vendaItem);

                if (dados)
                {
                    var venda = new VendaBLL().ConsultarVenda(vendaItem.VendaId).First();
                    venda.Ven_total = ConsultarVendaItem(vendaItem.VendaId).ToList().Sum(x => x.VenIte_subtotal);

                    dados = new VendaDAL().AtualizarVenda(venda);
                }

                return dados;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public bool RemoverVendaItem(int VendaId, int id)
        {
            try
            {
                var vendaItem = ConsultarVendaItem(VendaId, id).FirstOrDefault();
                var dados = new VendaItemDAL().RemoverVendaItem(vendaItem);

                if (dados)
                {
                    var venda = new VendaBLL().ConsultarVenda(vendaItem.VendaId).First();
                    venda.Ven_total = ConsultarVendaItem(vendaItem.VendaId).ToList().Sum(x => x.VenIte_subtotal);

                    dados = new VendaDAL().AtualizarVenda(venda);
                }

                return dados;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
    }
}
