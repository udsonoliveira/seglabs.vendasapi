﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace apivendas.Migrations
{
    public partial class ComissaoVendedor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Ven_comissao",
                schema: "seglabs",
                table: "VENDEDOR",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Ven_comissao",
                schema: "seglabs",
                table: "VENDEDOR");
        }
    }
}
