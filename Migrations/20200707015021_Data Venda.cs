﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace apivendas.Migrations
{
    public partial class DataVenda : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Ven_data",
                schema: "seglabs",
                table: "VENDA",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Ven_data",
                schema: "seglabs",
                table: "VENDA");
        }
    }
}
