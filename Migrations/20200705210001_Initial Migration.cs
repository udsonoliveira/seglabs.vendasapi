﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace apivendas.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "seglabs");

            migrationBuilder.CreateTable(
                name: "PRODUTO",
                schema: "seglabs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Pro_descricao = table.Column<string>(maxLength: 100, nullable: true),
                    Pro_valor = table.Column<decimal>(nullable: false),
                    Pro_estoque = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PRODUTO", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VENDEDOR",
                schema: "seglabs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ven_nome = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VENDEDOR", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VENDA",
                schema: "seglabs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    VendedorId = table.Column<int>(nullable: false),
                    Ven_total = table.Column<decimal>(nullable: false),
                    Ven_finalizada = table.Column<bool>(nullable: false),
                    Ven_obs = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VENDA", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VENDA_VENDEDOR_VendedorId",
                        column: x => x.VendedorId,
                        principalSchema: "seglabs",
                        principalTable: "VENDEDOR",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VENDAITEM",
                schema: "seglabs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    VendaId = table.Column<int>(nullable: false),
                    ProdutoId = table.Column<int>(nullable: false),
                    VenIte_quantidade = table.Column<decimal>(nullable: false),
                    VenIte_subtotal = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VENDAITEM", x => new { x.Id, x.VendaId });
                    table.ForeignKey(
                        name: "FK_VENDAITEM_PRODUTO_ProdutoId",
                        column: x => x.ProdutoId,
                        principalSchema: "seglabs",
                        principalTable: "PRODUTO",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VENDAITEM_VENDA_VendaId",
                        column: x => x.VendaId,
                        principalSchema: "seglabs",
                        principalTable: "VENDA",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VENDA_VendedorId",
                schema: "seglabs",
                table: "VENDA",
                column: "VendedorId");

            migrationBuilder.CreateIndex(
                name: "IX_VENDAITEM_ProdutoId",
                schema: "seglabs",
                table: "VENDAITEM",
                column: "ProdutoId");

            migrationBuilder.CreateIndex(
                name: "IX_VENDAITEM_VendaId",
                schema: "seglabs",
                table: "VENDAITEM",
                column: "VendaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VENDAITEM",
                schema: "seglabs");

            migrationBuilder.DropTable(
                name: "PRODUTO",
                schema: "seglabs");

            migrationBuilder.DropTable(
                name: "VENDA",
                schema: "seglabs");

            migrationBuilder.DropTable(
                name: "VENDEDOR",
                schema: "seglabs");
        }
    }
}
