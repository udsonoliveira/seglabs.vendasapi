﻿using System;
using System.Linq;
using System.Net;
using VendasApi.Code;
using VendasApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace VendasApi.Controllers
{
    [ApiController]
    [Route("Api/[controller]")]    
    public class VendaController : ControllerBase
    {
        #region METODOS

        /// <summary>
        /// Consultar venda
        /// </summary>
        /// <returns></returns>
        /// <response code="200">OK</response>
        /// <response code="204">Nenhum resultado encontrado</response>        
        /// <response code="500">Falha na execução do serviço</response>
        [HttpGet]
        [Route("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]        
        public IActionResult Venda(int id = 0, int vendedorId = 0, DateTime? Ven_data = null)
        {
            try
            {
                var dados = new VendaBLL().ConsultarVenda(id, vendedorId, Ven_data);
                if(dados.Count > 0)
                {
                    return Ok(dados);
                }
                else
                {
                    return NoContent();
                }
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Cadastrar Venda
        /// </summary>
        /// <returns></returns>
        /// <response code="201">Cadastrado</response>
        /// <response code="412">Dados informados inválidos</response>
        /// <response code="500">Falha na execução do serviço</response>
        [HttpPost]
        [Route("")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status412PreconditionFailed)]
        public IActionResult CadastrarVenda([FromBody] Venda venda)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var dados = new VendaBLL().CadastrarVenda(venda);
                    return StatusCode((int)HttpStatusCode.Created, dados);
                }
                else
                {
                    return StatusCode((int)HttpStatusCode.PreconditionFailed, ModelState.SelectMany(x => x.Value.Errors));
                }
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Atualizar venda
        /// </summary>
        /// <returns></returns>
        /// <response code="200">OK</response>
        /// <response code="412">Dados informados inválidos</response>
        /// <response code="500">Falha na execução do serviço</response>
        [HttpPut]
        [Route("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status412PreconditionFailed)]
        public IActionResult AtualizarVenda([FromBody] Venda venda)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var dados = new VendaBLL().AtualizarVenda(venda);
                    return Ok(dados);
                }
                else
                {
                    return StatusCode((int)HttpStatusCode.PreconditionFailed, ModelState.SelectMany(x => x.Value.Errors));
                }
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        #endregion
    }
}
