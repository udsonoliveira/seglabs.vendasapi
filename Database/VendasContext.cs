﻿using VendasApi.Helpers;
using VendasApi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Linq;

namespace VendasApi.Database
{
    public class VendasContext : DbContext
    {
        #region Construtores
        public VendasContext() { }
        #endregion

        #region Métodos

        /// <summary>
        /// Gravar informações no banco de dados informando o valor do ID
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enableIdentityInsert"></param>
        /// <returns></returns>
        public int SaveChanges<T>(bool enableIdentityInsert) where T : class
        {
            var retorno = 0;

            try
            {
                var entityTypeOfT = base.Model.GetEntityTypes().First(t => t.ClrType == typeof(T));
                var tableNameAnnotation = entityTypeOfT.GetAnnotation("Relational:TableName");
                var schemaNameAnnotation = entityTypeOfT.GetAnnotation("Relational:Schema");

                if (enableIdentityInsert)
                {
                    var table = tableNameAnnotation.Value.ToString();
                    var schema = schemaNameAnnotation.Value.ToString();

                    base.Database.OpenConnection();
                    try
                    {
                        base.Database.ExecuteSqlRaw($"SET IDENTITY_INSERT [{schema}].[{table}] ON");
                        retorno = base.SaveChanges();
                        base.Database.ExecuteSqlRaw($"SET IDENTITY_INSERT [{schema}].[{table}] OFF");
                    }
                    finally
                    {
                        base.Database.CloseConnection();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            
            return retorno;
        }      

        /// <summary>
        /// Método de configuração inicial do contexto
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(this.DbConnection());
        }
        
        /// <summary>
        /// Método que retorna a string de conexão parametrizada
        /// </summary>
        /// <returns></returns>
        private string DbConnection()
        {
            var app = new AppSettingsManager();
            return $"Server={app.GetValue("DbConnection:Server")};" +
                   $"Database={app.GetValue("DbConnection:Database")};" +
                   $"User Id={app.GetValue("DbConnection:User")};" +
                   $"Password={app.GetValue("DbConnection:Password")};" +
                   $"Integrated Security={app.GetValue("DbConnection:Options:IntegratedSecurity")};" +
                   $"MultipleActiveResultSets={app.GetValue("DbConnection:Options:MultipleActiveResultSets")};" +
                   $"encrypt={app.GetValue("DbConnection:Options:encrypt")};" +
                   $"TrustServerCertificate={app.GetValue("DbConnection:Options:TrustServerCertificate")}";
        }

        /// <summary>
        /// Método de modificação dos modelos
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<VendaItem>()
                .HasKey(x => new { x.Id, x.VendaId });

                modelBuilder
                    .Entity<VendaItem>()
                    .Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .UseIdentityColumn();

            base.OnModelCreating(modelBuilder);
        }

        #endregion

        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<VendaItem> VendaItens { get; set; }

    }
}