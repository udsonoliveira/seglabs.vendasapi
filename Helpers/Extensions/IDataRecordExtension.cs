﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace VendasApi.Helpers.Extensions 
{ 
    /// <summary>
    /// CLASSE DE CONTROLE DE EXTENSÕES PARA LEITURA DE UM IDATAREADER
    /// </summary>
    public static class IDataRecordExtension
    {
        /// <summary>
        /// Converte o data reader em um objeto específico.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dr"></param>
        /// <returns></returns>
        public static List<T> ConverterDataReaderParaObjeto<T>(this IDataReader dr)
        {
            var linha = 0;
            try
            {
                List<T> listaGenerica = new List<T>();
                T objetoGenerico = default;                

                while (dr.Read())
                {
                    objetoGenerico = Activator.CreateInstance<T>();
                    var propriedades = objetoGenerico.GetType().GetProperties();

                    for (var i = 0; i < dr.FieldCount; i++)
                    {
                        var nomeColuna = dr.GetName(i);
                        var propriedadeCorrente = propriedades.Where(p => p.Name == nomeColuna).FirstOrDefault();
                        if (propriedadeCorrente != null && !object.Equals(dr[propriedadeCorrente.Name], DBNull.Value))
                        {
                            propriedadeCorrente.SetValue(objetoGenerico, dr[propriedadeCorrente.Name].ConverterPara(propriedadeCorrente), null);
                        }
                    }

                    listaGenerica.Add(objetoGenerico);
                    linha++;
                }
             
                return listaGenerica;
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Erro de conversão de data reader na linha {0} Erro: {1}.", linha, ex.Message));
            }
        }
    }
}