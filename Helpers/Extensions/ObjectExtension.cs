﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Data;
using System.Linq;
using System;
using System.Data.SqlClient;

namespace VendasApi.Helpers.Extensions
{
    /// <summary>
    /// Classe de extensão para objetos, possibilitando conversões e tratamentos dinâmicos.
    /// </summary>
    public static class ObjectExtension
    {

        #region CONVERSÃO

        /// <summary>
        /// Conversão de um valor para uma propriedade específica.
        /// </summary>
        /// <param name="valor">Valor do campo.</param>
        /// <param name="propriedadeCorrente">Informações da prorpriedade para tratamento.</param>
        /// <returns></returns>
        public static object ConverterPara(this object valor, PropertyInfo propriedadeCorrente)
        {
            try
            {
                Type t = Nullable.GetUnderlyingType(propriedadeCorrente.PropertyType) ?? propriedadeCorrente.PropertyType;
                return (valor == null) ? null : Convert.ChangeType(valor, t);
            }
            catch (Exception)
            {
                throw new Exception(String.Format("O valor {0} não pode ser convertido no tipo {1} para a propriedade {2}.", valor, propriedadeCorrente.PropertyType, propriedadeCorrente.Name));
            }
        }

        /// <summary>
        /// Conversão de um valor para um tipo específico.
        /// </summary>
        /// <param name="valor">Valor do campo.</param>
        /// <param name="tipo">Tipo a ser convertido.</param>
        /// <param name="nomeColuna">Nome da coluna que está sendo convertida.</param>
        /// <returns></returns>
        public static object ConverterPara(this object valor, Type tipo, string nomeColuna)
        {
            try
            {
                Type t = Nullable.GetUnderlyingType(tipo) ?? tipo;
                return (valor == null) ? null : Convert.ChangeType(valor, t);
            }
            catch (Exception)
            {
                throw new Exception(String.Format("O valor {0} não pode ser convertido no tipo {1}. Coluna: {2}.", valor, tipo, nomeColuna));
            }
        }

        /// <summary>
        /// Conversão de um valor para um tipo específico.
        /// </summary>
        /// <param name="valor">Tipo a ser convertido.</param>
        /// <returns></returns>
        public static T ConverterPara<T>(this object valor)
        {
            try
            {
                TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));
                return (T)converter.ConvertFromString(null, CultureInfo.CurrentCulture, valor.ToString());
            }
            catch (Exception)
            {
                throw new Exception(String.Format("O valor {0} não pode ser convertido no tipo {1}.", valor ?? "null", typeof(T)));
            }
        }

        /// <summary>
        /// Passa o tipo de objeto do sistema e obtém o tipo de objeto da base de dados.
        /// </summary>
        /// <param name="tipoSistema"></param>
        /// <returns></returns>
        public static SqlDbType ObterTipoBD(this object tipoSistema)
        {
            var typeMap = new Dictionary<Type, SqlDbType>
            {
                [typeof(string)] = SqlDbType.NVarChar,
                [typeof(char[])] = SqlDbType.NVarChar,
                [typeof(int)] = SqlDbType.Int,
                [typeof(Int32)] = SqlDbType.Int,
                [typeof(Int16)] = SqlDbType.SmallInt,
                [typeof(Int64)] = SqlDbType.BigInt,
                [typeof(Byte[])] = SqlDbType.VarBinary,
                [typeof(Boolean)] = SqlDbType.Bit,
                [typeof(DateTime)] = SqlDbType.DateTime2,
                [typeof(DateTimeOffset)] = SqlDbType.DateTimeOffset,
                [typeof(Decimal)] = SqlDbType.Decimal,
                [typeof(Double)] = SqlDbType.Float,
                [typeof(Decimal)] = SqlDbType.Money,
                [typeof(Byte)] = SqlDbType.TinyInt,
                [typeof(TimeSpan)] = SqlDbType.Time                                
            };

            return tipoSistema == null ? SqlDbType.NVarChar : typeMap[tipoSistema.GetType()];
        }

        #endregion

        #region CÓPIA

        public static void MergeObject<T>(this T destino, T origem)
        {
            Type t = typeof(T);

            var properties = t.GetProperties().Where(prop => prop.CanRead && prop.CanWrite);

            foreach (var prop in properties)
            {
                var value = prop.GetValue(origem, null);

                if (prop.PropertyType == typeof(DateTime))
                {
                    if (Convert.ToDateTime(value) == DateTime.MinValue)
                    {
                        value = null;
                    }
                }

                if(prop.PropertyType.IsEnum)
                {
                    if((int)value == 0)
                    {
                        continue;
                    }
                }

                if (prop.PropertyType.GetProperties().Where(x => x.CanRead && x.CanWrite).Count() > 0)
                {
                    continue;
                }

                if (value != null)
                {
                    prop.SetValue(destino, value, null);
                }
            }
        }

        public static void MergeObjectForEach<T>(this List<T> destino, List<T> origem) where T : class
        {
            Type t = typeof(T);
            var property = t.GetProperties().Where(prop => prop.Name == "Id").FirstOrDefault();

            foreach(var o in origem)
            {                
                if (property != null)
                {
                    var idProp = typeof(T).GetProperty("Id");
                    var idValue = idProp.GetValue(o, null);
                    var result = destino.Where(x => idProp.GetValue(x).Equals(idValue)).FirstOrDefault();
                 
                    if(result != null)
                    {
                        result.MergeObject(o);
                    }
                    else
                    {
                        destino.Add(o);
                    }
                }
            }            
        }

        #endregion

        #region PARÂMETRO

        /// <summary>
        /// Adiciona o parâmetro para acesso ao banco de dados.
        /// </summary>
        /// <param name="campo"></param>
        /// <param name="nomeParametro"></param>
        /// <param name="baseDados"></param>
        /// <param name="formato"></param>
        public static void AddParameter(this object campo, string nomeParametro, SqlCommand baseDados, string formato = null)
        {
            try
            {
                SqlDbType? tipoCampo = null;

                if (campo != null)
                {
                    if (campo.GetType().IsEnum)
                    {
                        tipoCampo = SqlDbType.Int;
                    }
                    else
                    {
                        tipoCampo = campo.ObterTipoBD();
                    }

                    if (!String.IsNullOrEmpty(formato) && tipoCampo == SqlDbType.DateTime)
                    {                        
                        tipoCampo = SqlDbType.NVarChar;
                        baseDados.Parameters.Add($"@{nomeParametro}", (SqlDbType)tipoCampo);
                        baseDados.Parameters[$"@{nomeParametro}"].Value = Convert.ToDateTime(campo).ToString(formato);                        
                    }
                    else if (campo.GetType().IsEnum)
                    {
                        baseDados.Parameters.Add($"@{nomeParametro}", (SqlDbType)tipoCampo);
                        baseDados.Parameters[$"@{nomeParametro}"].Value = (int)campo;                        
                    }
                    else
                    {
                        baseDados.Parameters.Add($"@{nomeParametro}", (SqlDbType)tipoCampo);
                        baseDados.Parameters[$"@{nomeParametro}"].Value = campo;
                    }
                }
                else
                {
                    baseDados.Parameters.Add($"@{nomeParametro}", SqlDbType.VarChar);
                    baseDados.Parameters[$"@{nomeParametro}"].Value = DBNull.Value;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Ocorreu uma falha ao tentar adicionar o parâmetro {0}. Exceção: {1}.", campo.GetType().Name, ex.Message));
            }
        }

        #endregion
    }
}
