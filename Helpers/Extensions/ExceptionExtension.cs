﻿using System;
using System.Diagnostics;

namespace VendasApi.Helpers.Extensions
{

    /// <summary>
    /// Extensões da exceção.
    /// </summary>
    public static class ExceptionExtension
    {
        #region TRATAMENTO

        /// <summary>
        /// Tratamento de exceção.
        /// </summary>
        /// <param name="excecao"></param>
        /// <returns></returns>
        public static Exception HandleException(this Exception excecao)
        {
            var stackTrace = new StackTrace();
            var classe = stackTrace.GetFrame(1).GetMethod().ReflectedType.Name;
            var metodo = stackTrace.GetFrame(1).GetMethod().Name;
            return new Exception(string.Format("Falha na execução {0}({1}). Mensagem: {2}. InnerException: {3}", classe, metodo, excecao.Message, excecao.InnerException.Message));
        }

        #endregion
    }

}