﻿using Microsoft.Extensions.Configuration;
using System;

namespace VendasApi.Helpers
{
    public class AppSettingsManager
    {
        public string GetValue(string value)
        {
            IConfigurationRoot Configuration = new ConfigurationBuilder()
                                      .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                                      .AddJsonFile("appsettings.json")
                                      .Build();

            return Configuration.GetSection(value).Value;
        }
    }
}
