﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

namespace VendasApi.Models
{
    [Table("PRODUTO", Schema = "seglabs")]
    public class Produto
    {
        /// <summary>
        /// Código de identificação do produto
        /// </summary>
        [Display(Name = "Código")]
        [Range(0, 9999999999, ErrorMessage = "O valor de {0} deve estar entre {1} e {2}")]
        public int Id { get; set; }

        /// <summary>
        /// Descrição do produto
        /// </summary>
        [Display(Name = "Descrição")]
        [StringLength(100, MinimumLength = 0, ErrorMessage = ("O tamanho de {0} deve estar entre {1} e {2}"))]
        public string Pro_descricao { get; set; }

        /// <summary>
        /// Valor de venda do produto
        /// </summary>
        [Display(Name = "Valor")]
        [DataType(DataType.Currency)]
        public decimal Pro_valor { get; set; }

        /// <summary>
        /// Estoque atual do produto
        /// </summary>
        [Display(Name = "Estoque")]
        [RegularExpression(@"^(0|-?\d{0,16}(\.\d{0,2})?)$")]
        [Range(0, 9999999999999999.99)]
        public decimal Pro_estoque { get; set; }
    }
}
