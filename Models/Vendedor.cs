﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VendasApi.Models
{
    [Table("VENDEDOR", Schema = "seglabs")]
    public class Vendedor
    {
        /// <summary>
        /// Código de identificação do produto
        /// </summary>
        [Display(Name = "Código")]
        [Range(0, 9999999999, ErrorMessage = "O valor de {0} deve estar entre {1} e {2}")]
        public int Id { get; set; }

        /// <summary>
        /// Nome do Vendedor
        /// </summary>
        [Display(Name = "Nome")]
        [StringLength(100, MinimumLength = 0, ErrorMessage = ("O tamanho de {0} deve estar entre {1} e {2}"))]
        public string Ven_nome { get; set; }

        /// <summary>
        /// Nome do Vendedor
        /// </summary>
        [Display(Name = "Comissão")]
        [DataType(DataType.Currency)]
        public decimal Ven_comissao { get; set; }
    }
}
