﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

namespace VendasApi.Models
{
    [Table("VENDAITEM", Schema = "seglabs")]
    public class VendaItem
    {
        /// <summary>
        /// Código de identificação do Item
        /// </summary>
        [Display(Name = "Código")]
        [Range(0, 9999999999, ErrorMessage = "O valor de {0} deve estar entre {1} e {2}")]
        public int Id { get; set; }

        /// <summary>
        /// Venda a qual o item pertence
        /// </summary>
        [Display(Name = "Venda")]
        [ForeignKey("VendaId")]
        public Venda VenIte_venda { get; set; }

        /// <summary>
        /// Código de identificação da venda
        /// </summary>
        [Required]
        [Display(Name = "Código da Venda")]
        [Range(0, 9999999999, ErrorMessage = "O valor de {0} deve estar entre {1} e {2}")]
        public int VendaId { get; set; }

        /// <summary>
        /// Produto referente ao item
        /// </summary>
        [Display(Name = "Produto")]
        [ForeignKey("ProdutoId")]
        public Produto VenIte_produto { get; set; }

        /// <summary>
        /// Código de identificação da venda
        /// </summary>
        [Required]
        [Display(Name = "Código do Produto")]
        [Range(0, 9999999999, ErrorMessage = "O valor de {0} deve estar entre {1} e {2}")]
        public int ProdutoId { get; set; }

        /// <summary>
        /// Quantidade referente ao item
        /// </summary>
        [Display(Name = "Estoque")]
        [RegularExpression(@"^\d+(\.\d{1,2})?$")]
        [Range(0, 9999999999999999.99)]
        public decimal VenIte_quantidade { get; set; }

        /// <summary>
        /// Valor de venda do produto
        /// </summary>
        [Display(Name = "Subtotal")]
        [DataType(DataType.Currency)]
        public decimal VenIte_subtotal { get; set; }
    }
}
