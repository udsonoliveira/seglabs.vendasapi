﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VendasApi.Models
{
    [Table("VENDA", Schema = "seglabs")]
    public class Venda
    {
        /// <summary>
        /// Código de identificação da venda
        /// </summary>
        [Display(Name = "Código")]
        [Range(0, 9999999999, ErrorMessage = "O valor de {0} deve estar entre {1} e {2}")]
        public int Id { get; set; }

        /// <summary>
        /// Vendedor responsável pela venda
        /// </summary>
        [Display(Name = "Vendedor")]
        [ForeignKey("VendedorId")]
        public Vendedor Ven_vendedor { get; set; }

        /// <summary>
        /// Código de identificação do vendedor
        /// </summary>
        [Required]
        [Display(Name = "Código do Vendedor")]
        [Range(0, 9999999999, ErrorMessage = "O valor de {0} deve estar entre {1} e {2}")]
        public int VendedorId { get; set; }

        /// <summary>
        /// Valor total da venda
        /// </summary>
        [Display(Name = "Data da Venda")]
        [DataType(DataType.DateTime)]
        public DateTime Ven_data { get; set; }

        /// <summary>
        /// Valor total da venda
        /// </summary>
        [Display(Name = "Total da Venda")]
        [DataType(DataType.Currency)]
        public decimal Ven_total { get; set; }

        /// <summary>
        /// Finalizada
        /// </summary>
        [Display(Name = "Finalizada")]
        public bool Ven_finalizada { get; set; }

        /// <summary>
        /// Observações gerais
        /// </summary>
        [Display(Name = "Observações")]
        [StringLength(500, MinimumLength = 0, ErrorMessage = ("O tamanho de {0} deve estar entre {1} e {2}"))]
        public string Ven_obs { get; set; }

    }
}
